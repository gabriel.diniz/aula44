public class aula44 { // array unidimensional

    public static void main(String[] args) {

    int[] array; //declaração do nosso array
    array = new int[10]; // cria e reserva o espaço para o nosso array
    // 0 a 9, 10 unidades
    for (int i = 0; i <= 9; i++) { // variavel inteira 0, i menor ou igual a 9, acrescenta unidade
        System.out.printf("%5d%7d\n", i, array[i]); // imprime 5d 5 casas, 7d 7 elementos
        }
    }
}
